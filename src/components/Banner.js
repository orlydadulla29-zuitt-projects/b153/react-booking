import { Row, Col, Button } from 'react-bootstrap'


export default function Banner(){
	return(
		<Row>
			<Col>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunites for everyone, everywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	)
}